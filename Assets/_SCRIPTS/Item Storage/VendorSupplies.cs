﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendorSupplies : MonoBehaviour
{
    // Use this for initialization
    //USED TO STORE LIST OF ITEMS FOR INDIVIDUAL VENDORS
    //using int to reference item ID 
    //-1 is used to denote no item
    public int[] supplies;
}
